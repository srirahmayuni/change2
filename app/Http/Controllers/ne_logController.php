<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class ne_logController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index(Request $request)
    {
        //ini_set('memory_limit','1024M');
        $page = $request->input('page');
        if ($page == null || $page == 0) {
            $nelog = DB::table('ne_log')
            ->selectRaw('
                netype,
                nename,
                neip,
                operator,
                terminal,
                mmlstart,
                mmlend,
                mml,
                message,
                domain
            ')
            ->limit(100)
            ->orderBy('mmlstart', 'desc')
            ->get();
        }else {
            $skip = ($page - 1) * 100;
            $nelog = DB::table('ne_log')
            ->selectRaw('
                netype,
                nename,
                neip,
                operator,
                terminal,
                mmlstart,
                mmlend,
                mml,
                message,
                domain
            ')
            ->skip($skip)
            ->limit(100)
            ->orderBy('mmlstart', 'desc')
            ->get();
        }


        return response()->json($nelog);
    }

    public function top_5_user_activity()
    {
        $count = DB::table('ne_log')
        ->select('operator', DB::raw('count(*) as total'))
        ->groupBy('operator')
        ->orderBy('total', 'desc')
        ->limit(5)
        ->get();

        return response()->json($count);
    }

    public function user_activity()
    {
        $now = Carbon::now();
        $a_month_before = $now->subMonth();

        $label = [
            $now->format('F Y'),
            $a_month_before->format('F Y')
        ];

        $now = Carbon::now();
        $user = DB::connection('mysql2')
        ->table('t_log_change')
        ->select('Operator')
        ->groupBy('Operator')
        ->get();

        $datasets_now = DB::connection('mysql2')
        ->table('t_log_change')
        ->whereMonth('MMLStartTime', $now->month)
        ->whereYear('MMLStartTime', $now->year)
        ->select(DB::raw('count(*) as total'))
        ->groupBy('Operator')
        ->get();
        $datasets_before = DB::connection('mysql2')
        ->table('t_log_change')
        ->whereMonth('MMLStartTime', $a_month_before->month)
        ->whereYear('MMLStartTime', $now->year)
        ->select(DB::raw('count(*) as total'))
        ->groupBy('Operator')
        ->get();

        $data = [
            'user' => $user,
            'datasets' => [
                $datasets_now,
                $datasets_before
            ]
        ];



        return response()->json($data);
    }

    public function mml_command($command)
    {
        $mml = DB::table('ne_log')
        ->where('mml', 'like', '%'.$command.' %')
        ->get();

        return response()->json($mml);
    }

    public function license_activated()
    {
        $license = DB::table('ne_log')
        // ->where('mml', 'like', '%ACT LICENSE%')
        ->select(DB::raw('DATE(mmlstart) as date'), DB::raw('SUM(CASE WHEN mml LIKE "%ACT LICENSE%" THEN 1 ELSE 0 END) as total'))
        ->orderBy('date', 'desc')
        ->groupBy('date')
        ->get();

        return response()->json($license);
    }

    public function cell_deactivated()
    {
        $license = DB::table('ne_log')
        ->select(DB::raw('DATE(mmlstart) as date'), DB::raw('SUM(CASE WHEN
        mml LIKE "%REM CELL%" OR
        mml LIKE "%REM BTS%" OR
        mml LIKE "%DEL CELL%" OR
        mml LIKE "%DEL BTS%" OR
        mml LIKE "%DEA CELL%" OR
        mml LIKE "%DEA BTS%" THEN 1 ELSE 0 END) as total'))
        ->orderBy('date', 'desc')
        ->groupBy('date')
        ->get();

        return response()->json($license);
    }

    // dafinci 1-12
    public function dafinci()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R1 Sumbagut%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci2()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R2 Sumbagsel%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci3()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region','like','R3 Jabotabek%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci4()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R4 West Java%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci5()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R5 Central Java%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci6()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R6 East Java%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci7()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R7 Bali Nusra%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci8()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R8 Kalimantan%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci9()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R9 Sulawesi%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci10()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R10 Sumbagteng%')
        ->count('NEid');

        return response()->json($data);
    }

    public function dafinci11()
    {
        $data = DB::table('t_dafinci_raw')
        ->where('Region', 'like', 'R11 Papua Maluku%')
        ->count('NEid');

        return response()->json($data);
    }
    //

    //remedy 1-11
    public function remedy()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R1 Sumbagut;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy2()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R2 Sumbagsel;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy3()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R3 Jabotabek;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy4()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R4 West Java;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy5()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R5 Central Java;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy6()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R6 East Java;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy7()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R7 Bali Nusra;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');


        return response()->json($data);
    }

    public function remedy8()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R8 Kalimantan;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy9()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R9 Sulawesi;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy10()
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R10 Sumbagteng;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }

    public function remedy11() 
    {
        $data = DB::connection('mysql2')
        ->table('t_remedy_new')
        ->where('REGION_IMPACTED','R11 Papua Maluku;;')
        ->where('STATUS','Completed')
        ->where('APPROVAL_STATUS','Approved')
        ->count('INFRASTRUCTURE_CHANGE_ID');

        return response()->json($data);
    }
    //

    //data bst on air for chart
    public function btsonair2g()
    {
        $date = Carbon::today()->subDays(7);

        $data = DB::connection('mysql2') 
        ->table('master_baseline_onair_2g')
        ->select(DB::raw('count(*) as neid, date_onair'))
        ->whereDate('date_onair','>=', $date)
        ->groupBy('date_onair')     
        ->limit(7)
        ->get();                    
        
        return response()->json($data);
    }

    public function btsonair3g()
    {
        $date = Carbon::today()->subDays(7);

        $data = DB::connection('mysql2') 
        ->table('master_baseline_onair_3g')
        ->select(DB::raw('count(*) as neid, date_onair'))
        ->whereDate('date_onair','>=', $date)
        ->groupBy('date_onair')     
        ->limit(7)
        ->get();                   
        
        return response()->json($data);
    }

    public function btsonair4g()
    {
        $date = Carbon::today()->subDays(7);

        $data = DB::connection('mysql2') 
        ->table('resume_nodin_bts')
        ->select(DB::raw('sum(total_4g) as neid, date as date_onair'))
        ->whereDate('date','>=', $date)
        ->groupBy('date')     
        ->limit(7)
        ->get();                   
        
        return response()->json($data);
    }
    
 
    //btsonair 1-11
    public function btsonair()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL1')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }


    public function btsonair2()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL2')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair3()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL3')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair4()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL4')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair5()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL5')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair6()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL6')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair7()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL7')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair8()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL8')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair9()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL9')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair10()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL10')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }

    public function btsonair11()
    {
        $data = DB::connection('mysql')
        ->table('t_sum_bts_nodeb_final')
        ->where('STATUS', 1)
        ->orWhere('STATUS', 2)
        ->where('REGIONAL', '=', 'REGIONAL11')
        // ->distinct('BTS_NODE_NAME')
        ->sum('NE_QTY');

        return response()->json($data);
    }
    //

    //nodin 1-11
    public function nodin()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r1')
        ->orWhere('Region_NE', '=', 'R1')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin2()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r2')
        ->orWhere('Region_NE', '=', 'R2')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin3()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r3')
        ->orWhere('Region_NE', '=', 'R3')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin4()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r4')
        ->orWhere('Region_NE', '=', 'R4')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin5()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', 'r5')
        ->orWhere('Region_NE', 'R5')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin6()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r6')
        ->orWhere('Region_NE', '=', 'R6')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin7()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r7')
        ->orWhere('Region_NE', '=', 'R7')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin8()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r8')
        ->orWhere('Region_NE', '=', 'R8')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin9()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->distinct('ne_id')
        ->where('Region_NE', '=', 'r9')
        ->orWhere('Region_NE', '=', 'R9')
        ->count('ne_id');

        return response()->json($data);
    }

    public function nodin10()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->where('Region_NE', '=', 'r10')
        ->orWhere('Region_NE', '=', 'R10')
        ->distinct('ne_id')
        ->count();

        return response()->json($data);
    }

    public function nodin11()
    {
        $data = DB::connection('mysql2')
        ->table('nodin')
        ->where('Region_NE', '=', 'r11')
        ->orWhere('Region_NE', '=', 'R11')
        ->distinct('ne_id')
        ->count();

        return response()->json($data);
    }
    //

    public function chart_user()
    {
        $time_start = microtime(true);
        $MMLStartTime = [
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        ];
        $MMLStartTime_for_send = [
            'January',
            'February',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'

        ];
        $hasil = [];
        $i=1;
        foreach ($MMLStartTime as $value) {
            $count = DB::connection('mysql2')
            ->table('t_log_change')
            ->whereBetween('MMLStartTime', ['2018-'.$i.'-01 00:00:01','2018-'.$i.'-31 23:59:00'])
            //->whereBetween('MMLStartTime', $value)
            ->count('MMLStartTime');

            array_push($hasil, $count);
            $i++;
        }
        $data = [
            'MMLStartTime' => $MMLStartTime_for_send,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);


    }

    public function chart_dea()
    {
        $time_start = microtime(true);
        $MMLStartTime = [
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''

        ];
        $MMLStartTime_for_send = [
            'January',
            'February',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'

        ];
        $hasil = [];
        $i=1;
        foreach ($MMLStartTime as $value) {
            $count = DB::connection('mysql2')
            ->table('t_log_change')
            ->select('MML')
            ->where('MML', 'like', '%dea cell%')
            ->whereBetween('MMLStartTime', ['2018-'.$i.'-01 00:00:01','2018-'.$i.'-31 23:59:00'])
            //->whereBetween('MMLStartTime', $value)
            ->count('MMLStartTime');

            array_push($hasil, $count);
            $i++;
        }
        $data = [
            'MMLStartTime' => $MMLStartTime_for_send,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

     public function chart_nodin()
    {
        $time_start = microtime(true);
        $kabupaten = [
            'Padang Sidempuan',
            'R2 Sumbangsel',
            'R3 Jabotabek'
        ];
        $kabupaten_for_send = [
            'R1',
            'R2',
            'R3',
        ];
        $hasil = [];
        foreach ($kabupaten as $value) {
            $count = DB::connection('mysql2')
            ->table('nodin')
            ->where('regional', $value)
            ->count('ne_id');

            array_push($hasil, $count);
        }
        $data = [
            'kabupaten' => $kabupaten_for_send,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function chart_btsonair()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil = [];
        foreach ($regional as $value) {
            $count = DB::connection('mysql')
            ->table('t_resume_sysinfo')
            ->selectRaw('(BTS + NODEB + ENODEB)')
            ->where('REGIONAL', $value)
            ->orderBy('date','DESC')
            ->limit(1)
            ->get();
            $bind = explode(':',$count);
            array_push($hasil, str_replace("}]","",$bind[1]));
        }
        $data = [
            'regional' => $regional_for_send,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    //active cell 1-11
    public function act_cell()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 1')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell2()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 2')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell3()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 3')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell4()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 4')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell5()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 5')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell6()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 6')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell7()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 7')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell8()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 8')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell9()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 9')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell10()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 10')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_cell11()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act cell%')
        ->where('regional', '=', 'Regional 11')
        ->count('*');

        return json_decode($data, true);
    }
    //

    //deactive cell 1-11
    public function dea_cell()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 1')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell2()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 2')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell3()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 3')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell4()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 4')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell5()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 5')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell6()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 6')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell7()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 7')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell8()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 8')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell9()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 9')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell10()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 10')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_cell11()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea cell%')
        ->where('regional', '=', 'Regional 11')
        ->count('*');

        return json_decode($data, true);
    }
    //

    //active license 1-11
    public function act_license()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 1')
        ->count('MML');

        return json_decode($data, true);
    }

    public function act_license2()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 2')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license3()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 3')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license4()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 4')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license5()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 5')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license6()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 6')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license7()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 7')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license8()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 8')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license9()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 9')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license10()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 10')
        ->count('*');

        return json_decode($data, true);
    }

    public function act_license11()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 11')
        ->count('*');

        return json_decode($data, true);
    }
    //

    //deactive license 1-11
    public function dea_license()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 1')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license2()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 2')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license3()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 3')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license4()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 4')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license5()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 5')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license6()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 6')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license7()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 7')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license8()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 8')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license9()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 9')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license10()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 10')
        ->count('*');

        return json_decode($data, true);
    }

    public function dea_license11()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%dea license%')
        ->where('regional', '=', 'Regional 11')
        ->count('*');

        return json_decode($data, true);
    }
    //

    //change 1-11
    public function add_bts()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 1')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts2()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 2')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts3()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 3')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts4()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 4')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts5()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 5')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts6()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 6')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts7()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 7')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts8()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 8')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts9()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 9')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts10()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 10')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }

    public function add_bts11()
    {
        $data = DB::connection('mysql2')
        ->table('t_log_change')
        ->where('regional','Regional 11')
        ->where('MML','LIKE','%ADD_BTS%')
        ->orWhere('MML','LIKE','%ADD_UNODEBFUNCTION%')
        ->orWhere('MML','LIKE','%ADD_ENODEB%')
        ->count('MML');

        return json_decode($data, true);
    }
    //

    //license 1-11
    public function add_license()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 1')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license2()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 2')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license3()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 3')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license4()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 4')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license5()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 5')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license6()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 6')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license7()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 7')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license8()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 8')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license9()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 9')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license10()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 10')
        ->count('*');

        return json_decode($data, true);
    }

    public function add_license11()
    {
        $data = DB::connection('mysql2')
        ->table("t_log_change")
        ->select('MML')
        ->where('MML', 'like', '%act license%')
        ->where('regional', '=', 'Regional 11')
        ->count('*');

        return json_decode($data, true);
    }
    //

    //change2
    public function plan_remedy()
    {
        $time_start = microtime(true);
        $kabupaten = [
            'Padang Sidempuan',
            'R2 Sumbangsel',
            'R3 Jabotabek',
            'R4 West Java;',
            'R5 Central Java;',
            'R6 East Java;',
            'R7 Bali Nusra;',
            'R8 Kalimantan;',
            'R9 Sulawesi;',
            'R10 Sumbagteng;',
            'R11 Papua Maluku;'
        ];
        $kabupaten_for_send1 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $regional = [
            'R1 Sumbagut;',
            'R2 Sumbagsel;',
            'R3 Jabotabek;',
            'R4 West Java;',
            'R5 Central Java;',
            'R6 East Java;',
            'R7 Bali Nusra;',
            'R8 Kalimantan;',
            'R9 Sulawesi;',
            'R10 Sumbagteng;',
            'R11 Papua Maluku;'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($kabupaten as $value1) {
            $count1 = DB::connection('mysql2')
            ->table('nodin')
            ->where('regional', $value1)
            ->count('ne_id');

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional as $value2) {
            $count2 = DB::connection('mysql')
            ->table('t_remedy_raw')
            ->where('Region', $value2)
            ->count('NetworkElementName');

            array_push($hasil2, $count2);
        }
        $data = [
            'regional' => $regional_for_send2,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function plan_onair()
    {
        $time_start = microtime(true);
        $kabupaten = [
            'R1 Sumbagut',
            'R2 Sumbangsel',
            'R3 Jabotabek',
            'R4 West Java',
            'R5 Central Java',
            'R6 East Java',
            'R7 Bali Nusra',
            'R8 Kalimantan',
            'R9 Sulawesi',
            'R10 Sumbagteng',
            'R11 Papua Maluku'
        ];
        $kabupaten_for_send1 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $i = 0;
        $hasil1 = [233,287,453,150,264,226,299,100,400,363,50];
        $hasil2 = [];
        foreach ($kabupaten as $value1) {
            $count1 = DB::connection('mysql')
            ->table('t_resume_sysinfo')
            ->selectRaw('REGIONAL AS REGIONAL, bts + nodeb + enodeb AS TOTAL, SUBSTR(REGIONAL,9,2) AS filter_region')
            ->whereRaw('MONTH(date) = MONTH(NOW())')
            ->groupBy('REGIONAL')
            ->orderByRaw('LENGTH(REGIONAL) ASC')
            ->orderBy('filter_region','ASC')
            ->get();

            $count2 = DB::connection('mysql')
            ->table('t_resume_sysinfo')
            ->selectRaw('REGIONAL AS REGIONAL, bts + nodeb + enodeb AS TOTAL, SUBSTR(REGIONAL,9,2) AS filter_region')
            ->whereRaw('MONTH(date) = MONTH(NOW()) - 1')
            ->groupBy('REGIONAL')
            ->orderByRaw('LENGTH(REGIONAL) ASC')
            ->orderBy('filter_region','ASC')
            ->get();

            array_push($hasil2, $count1[$i]->TOTAL - $count2[$i]->TOTAL);
            $i++;
        }
        // foreach ($kabupaten as $value1) {
        //     $count1 = DB::connection('mysql')
        //     ->table('t_resume_sysinfo')
        //     ->selectRaw('REGIONAL AS REGIONAL, bts + nodeb + enodeb AS TOTAL, SUBSTR(REGIONAL,9,2) AS filter_region')
        //     ->whereRaw('MONTH(date) = MONTH(NOW())')
        //     ->groupBy('REGIONAL')
        //     ->orderByRaw('LENGTH(REGIONAL) ASC')
        //     ->orderBy('filter_region','ASC')
        //     ->get();
        //
        //     array_push($hasil1, $count1[$i]->TOTAL);
        //     $i++;
        // }
        $data = [
            'regional' => $regional_for_send2,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function neidkosong()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($regional as $value1) {
            $count1 = DB::connection('mysql')
            ->table('c_stylo_master_cell_2g')
            ->where('REGIONAL', $value1)
            ->where('NE_ID','')
            ->orWhere('NE_ID',null)
            ->count('NE_ID');

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional as $value2) {
            $count2 = DB::connection('mysql')
            ->table('c_stylo_master_cell_3g')
            ->where('REGIONAL', $value2)
            ->where('NE_ID','')
            ->orWhere('NE_ID',null)
            ->count('NE_ID');

            array_push($hasil2, $count2);
        }
        $hasil3 = [];
        foreach ($regional as $value3) {
            $count3 = DB::connection('mysql')
            ->table('c_stylo_master_cell_4g')
            ->where('REGIONAL', $value3)
            ->where('NE_ID','')
            ->orWhere('NE_ID',null)
            ->count('NE_ID');

            array_push($hasil3, $count3);
        }
        $data = [
            'regional' => $regional_for_send2,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2,
            'hasil3' => $hasil3
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function lackosong()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($regional as $value1) {
            $count1 = DB::connection('mysql')
            ->table('c_stylo_master_cell_2g')
            ->where('REGIONAL', $value1)
            ->where('LAC','')
            ->orWhere('NE_ID',null)
            ->count('LAC');

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional as $value2) {
            $count2 = DB::connection('mysql')
            ->table('c_stylo_master_cell_3g')
            ->where('REGIONAL', $value2)
            ->where('LAC','')
            ->orWhere('NE_ID',null)
            ->count('LAC');

            array_push($hasil2, $count2);
        }
        $hasil3 = [];
        foreach ($regional as $value3) {
            $count3 = DB::connection('mysql')
            ->table('c_stylo_master_cell_4g')
            ->where('REGIONAL', $value3)
            ->where('TAC','')
            ->orWhere('NE_ID',null)
            ->count('TAC');

            array_push($hasil3, $count3);
        }
        $data = [
            'regional' => $regional_for_send2,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2,
            'hasil3' => $hasil3
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function cikosong()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($regional as $value1) {
            $count1 = DB::connection('mysql')
            ->table('c_stylo_master_cell_2g')
            ->where('REGIONAL', $value1)
            ->where('CI','')
            ->orWhere('NE_ID',null)
            ->count('CI');

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional as $value2) {
            $count2 = DB::connection('mysql')
            ->table('c_stylo_master_cell_3g')
            ->where('REGIONAL', $value2)
            ->where('CI','')
            ->orWhere('NE_ID',null)
            ->count('CI');

            array_push($hasil2, $count2);
        }
        $hasil3 = [];
        foreach ($regional as $value3) {
            $count3 = DB::connection('mysql')
            ->table('c_stylo_master_cell_4g')
            ->where('REGIONAL', $value3)
            ->where('CI','')
            ->orWhere('NE_ID',null)
            ->count('CI');

            array_push($hasil3, $count3);
        }
        $data = [
            'regional' => $regional_for_send2,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2,
            'hasil3' => $hasil3
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function enodebid()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil = [];
        foreach ($regional as $value) {
            $count = DB::connection('mysql')
            ->table('c_stylo_master_cell_4g')
            ->where('REGIONAL', $value)
            ->where('ENODEBID','')
            ->orWhere('NE_ID',null)
            ->count('ENODEBID');

            array_push($hasil, $count);
        }
        $data = [
            'regional' => $regional_for_send2,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

        public function traffic_lac()
    {
        $time_start = microtime(true);

        $regional = [
            'BALI NUSRA',
            'SUMBAGUT',
            'SUMBAGTENG',
            'SUMBANGSEL',
            'JABOTABEK',
            'WEST JAVA',
            'CENTRAL JAVA',
            'EAST JAVA',
            'KALIMANTAN',
            'SULAWESI',
            'PUMA'
        ];
        $regional_for_send = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $regional2 = [
            'Bali Nusra',
            'Sumbagut',
            'Sumbagteng',
            'Sumbagsel',
            'Jabotabek',
            'West Java',
            'Central Java',
            'East Java',
            'Kalimantan',
            'Sulawesi',
            'Puma'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($regional2 as $value1) {
            $count1 = DB::connection('mysql')
            ->table('2g_traffic_sum')
            ->where('regional', $value1)
            ->where('lac', null)
            ->count();

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional2 as $value2) {
            $count2 = DB::connection('mysql')
            ->table('3g_payload_sum')
            ->where('REGIONAL', $value2)
            ->where('LAC', null)
            ->count();

            array_push($hasil2, $count2);
        }
        $hasil3 = [];
        foreach ($regional as $value3) {
            $count3 = DB::connection('mysql')
            ->table('4g_payload_sum')
            ->where('regional', $value3)
            ->where('tac', null)
            ->count();

            array_push($hasil3, $count3);
        }
        $data = [
            'regional' => $regional_for_send,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2,
            'hasil3' => $hasil3
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function traffic_ci()
    {
        $time_start = microtime(true);

        $regional = [
            'BALI NUSRA',
            'SUMBAGUT',
            'SUMBAGTENG',
            'SUMBANGSEL',
            'JABOTABEK',
            'WEST JAVA',
            'CENTRAL JAVA',
            'EAST JAVA',
            'KALIMANTAN',
            'SULAWESI',
            'PUMA'
        ];
        $regional_for_send = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $regional2 = [
            'Bali Nusra',
            'Sumbagut',
            'Sumbagteng',
            'Sumbagsel',
            'Jabotabek',
            'West Java',
            'Central Java',
            'East Java',
            'Kalimantan',
            'Sulawesi',
            'Puma'
        ];
        $regional_for_send2 = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil1 = [];
        foreach ($regional2 as $value1) {
            $count1 = DB::connection('mysql')
            ->table('2g_traffic_sum')
            ->where('regional', $value1)
            ->where('ci', null)
            ->count();

            array_push($hasil1, $count1);
        }
        $hasil2 = [];
        foreach ($regional2 as $value2) {
            $count2 = DB::connection('mysql')
            ->table('3g_payload_sum')
            ->where('REGIONAL', $value2)
            ->where('SAC', null)
            ->count();

            array_push($hasil2, $count2);
        }
        $hasil3 = [];
        foreach ($regional as $value3) {
            $count3 = DB::connection('mysql')
            ->table('4g_payload_sum')
            ->where('regional', $value3)
            ->where('cellid', null)
            ->count();

            array_push($hasil3, $count3);
        }
        $data = [
            'regional' => $regional_for_send,
            'hasil1' => $hasil1,
            'hasil2' => $hasil2,
            'hasil3' => $hasil3
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function datasalah()
    {
        $time_start = microtime(true);
        $regional = [
            'REGIONAL1',
            'REGIONAL2',
            'REGIONAL3',
            'REGIONAL4',
            'REGIONAL5',
            'REGIONAL6',
            'REGIONAL7',
            'REGIONAL8',
            'REGIONAL9',
            'REGIONAL10',
            'REGIONAL11'
        ];
        $regional_for_send = [
            'R1',
            'R2',
            'R3',
            'R4',
            'R5',
            'R6',
            'R7',
            'R8',
            'R9',
            'R10',
            'R11'
        ];
        $hasil = [];
        foreach ($regional as $value) {
           // $value1 = 'F1';
            //$value2 = 'F2';
            //$value3 = 'F3';

            $count = DB::connection('mysql')
            // //->whereRaw('RIGHT(NEID,2) NOT LIKE "%W%" or F1_F2_F3 != "F1" and RIGHT(NEID,2) NOT LIKE "%X%" or F1_F2_F3 != "F2" and RIGHT(NEID,2) NOT LIKE "%Z%" and F1_F2_F3 != "F3"')
            //->table('sysinfo_3g_baseline')
            // ->select("*",\DB::raw('CASE
            //      WHEN RIGHT(NEID,2) NOT LIKE "%W%" and F1_F2_F3 != "F1" THEN "SALAH"
            //      WHEN RIGHT(NEID,2) NOT LIKE "%X%" and F1_F2_F3 != "F2" THEN "SALAH"
            //      WHEN RIGHT(NEID,2) NOT LIKE "%Z%" and F1_F2_F3 != "F3" THEN "SALAH"
            //      END as FLAG'))
            // ->havingRaw('FLAG = "SALAH"')
            // ->get()

            ->select(DB::raw('select COUNT("*") as total FROM (SELECT CASE
                WHEN RIGHT(NEID,2) LIKE "%W%" AND F1_F2_F3 = "F1"
                THEN "BENAR"
                WHEN RIGHT(NEID,2) LIKE "%X%" AND F1_F2_F3 = "F2"
                THEN "BENAR"
                WHEN RIGHT(NEID,2) LIKE "%Z%" AND F1_F2_F3 = "F3"
                THEN "BENAR"
                ELSE "SALAH"
                END AS FLAG, REGIONAL FROM sysinfo_3g_baseline) sysinfo_3g_baseline
                where FLAG = "SALAH"  and REGIONAL = "'.$value.'"
                '));
            //->where('REGIONAL', $value);


            array_push($hasil, $count[0]->total);
        }
        $data = [
            'regional' => $regional_for_send,
            'hasil' => $hasil
        ];
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        return response()->json($data);
    }

    public function wrong_stylo()
        {
            $time_start = microtime(true);
            $regional = [
                'REGIONAL1',
                'REGIONAL2',
                'REGIONAL3',
                'REGIONAL4',
                'REGIONAL5',
                'REGIONAL6',
                'REGIONAL7',
                'REGIONAL8',
                'REGIONAL9',
                'REGIONAL10',
                'REGIONAL11'
            ];
            $regional_for_send = [
                'R1',
                'R2',
                'R3',
                'R4',
                'R5',
                'R6',
                'R7',
                'R8',
                'R9',
                'R10',
                'R11'
            ];
            $hasil = [];
            foreach ($regional as $value) {
                $value1 = 'F1';
                $value2 = 'F2';
                $value3 = 'F3';

                $count = DB::connection('mysql')
                ->select(DB::raw('select COUNT("*") AS total FROM (SELECT CASE
                WHEN RIGHT(NE_ID,2) LIKE "%W%" AND F1_F2_F3 = "'.$value1.'"
                THEN "BENAR"
                WHEN RIGHT(NE_ID,2) LIKE "%X%" AND F1_F2_F3 = "'.$value2.'"
                THEN "BENAR"
                WHEN RIGHT(NE_ID,2) LIKE "%Z%" AND F1_F2_F3 = "'.$value3.'"
                THEN "BENAR"
                ELSE "SALAH"
                END AS FLAG, REGIONAL FROM c_stylo_master_cell_3g) c_stylo_master_cell_3g
                where FLAG = "SALAH"  and REGIONAL = "'.$value.'"
                '));

                array_push($hasil, $count[0]->total);
            }
            $data = [
                'regional' => $regional_for_send,
                'hasil' => $hasil
            ];
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            return response()->json($data);
        }
}
