<?php

return [

   'default' => 'mysql',

   'connections' => [
        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
         ],

        'mysql2' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST2'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_DATABASE2'),
            'username'  => env('DB_USERNAME2'),
            'password'  => env('DB_PASSWORD2'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
            'mysql3' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST3'),
            'port'      => env('DB_PORT3'),
            'database'  => env('DB_DATABASE3'),
            'username'  => env('DB_USERNAME3'),
            'password'  => env('DB_PASSWORD3'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],
    ],
];
