<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('all_mml', 'ne_logController@index');
$router->get('top_5_user_activity', 'ne_logController@top_5_user_activity');
$router->get('user_activity', 'ne_logController@user_activity');
$router->get('mml_command/{command}', 'ne_logController@mml_command');
$router->get('license_activated', 'ne_logController@license_activated');
$router->get('cell_deactivated', 'ne_logController@cell_deactivated');

//change 1-11
$router->get('change','ne_logController@change');
$router->get('change1','ne_logController@change1');
$router->get('change2','ne_logController@change2');
$router->get('change3','ne_logController@change3');
$router->get('change4','ne_logController@change4');
$router->get('change5','ne_logController@change5');
$router->get('change6','ne_logController@change6');
$router->get('change7','ne_logController@change7');
$router->get('change8','ne_logController@change8');
$router->get('change9','ne_logController@change9');
$router->get('change10','ne_logController@change10');
$router->get('change11','ne_logController@change11');

//dafinci 1-11
$router->get('dafinci', 'ne_logController@dafinci');
$router->get('dafinci2', 'ne_logController@dafinci2');
$router->get('dafinci3', 'ne_logController@dafinci3');
$router->get('dafinci4', 'ne_logController@dafinci4');
$router->get('dafinci5', 'ne_logController@dafinci5');
$router->get('dafinci6', 'ne_logController@dafinci6');
$router->get('dafinci7', 'ne_logController@dafinci7');
$router->get('dafinci8', 'ne_logController@dafinci8');
$router->get('dafinci9', 'ne_logController@dafinci9');
$router->get('dafinci10', 'ne_logController@dafinci10');
$router->get('dafinci11', 'ne_logController@dafinci11');
//

//remedy 1-11
$router->get('remedy', 'ne_logController@remedy');
$router->get('remedy2', 'ne_logController@remedy2');
$router->get('remedy3', 'ne_logController@remedy3');
$router->get('remedy4', 'ne_logController@remedy4');
$router->get('remedy5', 'ne_logController@remedy5');
$router->get('remedy6', 'ne_logController@remedy6');
$router->get('remedy7', 'ne_logController@remedy7');
$router->get('remedy8', 'ne_logController@remedy8');
$router->get('remedy9', 'ne_logController@remedy9');
$router->get('remedy10', 'ne_logController@remedy10');
$router->get('remedy11', 'ne_logController@remedy11');
//

//btsonair 1-11
$router->get('btsonair', 'ne_logController@btsonair');
$router->get('btsonair2', 'ne_logController@btsonair2');
$router->get('btsonair3', 'ne_logController@btsonair3');
$router->get('btsonair4', 'ne_logController@btsonair4');
$router->get('btsonair5', 'ne_logController@btsonair5');
$router->get('btsonair6', 'ne_logController@btsonair6');
$router->get('btsonair7', 'ne_logController@btsonair7');
$router->get('btsonair8', 'ne_logController@btsonair8');
$router->get('btsonair9', 'ne_logController@btsonair9');
$router->get('btsonair10', 'ne_logController@btsonair10');
$router->get('btsonair11', 'ne_logController@btsonair11');
//

//nodin 1-11
$router->get('nodin', 'ne_logController@nodin');
$router->get('nodin2', 'ne_logController@nodin2');
$router->get('nodin3', 'ne_logController@nodin3');
$router->get('nodin4', 'ne_logController@nodin4');
$router->get('nodin5', 'ne_logController@nodin5');
$router->get('nodin6', 'ne_logController@nodin6');
$router->get('nodin7', 'ne_logController@nodin7');
$router->get('nodin8', 'ne_logController@nodin8');
$router->get('nodin9', 'ne_logController@nodin9');
$router->get('nodin10', 'ne_logController@nodin10');
$router->get('nodin11', 'ne_logController@nodin11');
//

//license 1-11
$router->get('add_license', 'ne_logController@add_license');
$router->get('add_license2', 'ne_logController@add_license2');
$router->get('add_license3', 'ne_logController@add_license3');
$router->get('add_license4', 'ne_logController@add_license4');
$router->get('add_license5', 'ne_logController@add_license5');
$router->get('add_license6', 'ne_logController@add_license6');
$router->get('add_license7', 'ne_logController@add_license7');
$router->get('add_license8', 'ne_logController@add_license8');
$router->get('add_license9', 'ne_logController@add_license9');
$router->get('add_license10', 'ne_logController@add_license10');
$router->get('add_license11', 'ne_logController@add_license11');
//

//btsonair 1-11
$router->get('add_bts', 'ne_logController@add_bts');
$router->get('add_bts2', 'ne_logController@add_bts2');
$router->get('add_bts3', 'ne_logController@add_bts3');
$router->get('add_bts4', 'ne_logController@add_bts4');
$router->get('add_bts5', 'ne_logController@add_bts5');
$router->get('add_bts6', 'ne_logController@add_bts6');
$router->get('add_bts7', 'ne_logController@add_bts7');
$router->get('add_bts8', 'ne_logController@add_bts8');
$router->get('add_bts9', 'ne_logController@add_bts9');
$router->get('add_bts10', 'ne_logController@add_bts10');
$router->get('add_bts11', 'ne_logController@add_bts11');
//

$router->get('chart_btsonair', 'ne_logController@chart_btsonair');
$router->get('chart_dea', 'ne_logController@chart_dea');
$router->get('chart_nodin', 'ne_logController@chart_nodin');
$router->get('chart_user', 'ne_logController@chart_user');

//active cell 1-11
$router->get('act_cell', 'ne_logController@act_cell');
$router->get('act_cell2', 'ne_logController@act_cell2');
$router->get('act_cell3', 'ne_logController@act_cell3');
$router->get('act_cell4', 'ne_logController@act_cell4');
$router->get('act_cell5', 'ne_logController@act_cell5');
$router->get('act_cell6', 'ne_logController@act_cell6');
$router->get('act_cell7', 'ne_logController@act_cell7');
$router->get('act_cell8', 'ne_logController@act_cell8');
$router->get('act_cell9', 'ne_logController@act_cell9');
$router->get('act_cell10', 'ne_logController@act_cell10');
$router->get('act_cell11', 'ne_logController@act_cell11');
//

//deactive cell 1-11
$router->get('dea_cell', 'ne_logController@dea_cell');
$router->get('dea_cell2', 'ne_logController@dea_cell2');
$router->get('dea_cell3', 'ne_logController@dea_cell3');
$router->get('dea_cell4', 'ne_logController@dea_cell4');
$router->get('dea_cell5', 'ne_logController@dea_cell5');
$router->get('dea_cell6', 'ne_logController@dea_cell6');
$router->get('dea_cell7', 'ne_logController@dea_cell7');
$router->get('dea_cell8', 'ne_logController@dea_cell8');
$router->get('dea_cell9', 'ne_logController@dea_cell9');
$router->get('dea_cell10', 'ne_logController@dea_cell10');
$router->get('dea_cell11', 'ne_logController@dea_cell11');
//

//active license 1-11
$router->get('act_license', 'ne_logController@act_license');
$router->get('act_license2', 'ne_logController@act_license2');
$router->get('act_license3', 'ne_logController@act_license3');
$router->get('act_license4', 'ne_logController@act_license4');
$router->get('act_license5', 'ne_logController@act_license5');
$router->get('act_license6', 'ne_logController@act_license6');
$router->get('act_license7', 'ne_logController@act_license7');
$router->get('act_license8', 'ne_logController@act_license8');
$router->get('act_license9', 'ne_logController@act_license9');
$router->get('act_license10', 'ne_logController@act_license10');
$router->get('act_license11', 'ne_logController@act_license11');
//

//deactive license
$router->get('dea_license', 'ne_logController@dea_license');
$router->get('dea_license2', 'ne_logController@dea_license2');
$router->get('dea_license3', 'ne_logController@dea_license3');
$router->get('dea_license4', 'ne_logController@dea_license4');
$router->get('dea_license5', 'ne_logController@dea_license5');
$router->get('dea_license6', 'ne_logController@dea_license6');
$router->get('dea_license7', 'ne_logController@dea_license7');
$router->get('dea_license8', 'ne_logController@dea_license8');
$router->get('dea_license9', 'ne_logController@dea_license9');
$router->get('dea_license10', 'ne_logController@dea_license10');
$router->get('dea_license11', 'ne_logController@dea_license11');
//

$router->get('neidkosong', 'ne_logController@neidkosong');
$router->get('plan_remedy', 'ne_logController@plan_remedy');
$router->get('plan_onair', 'ne_logController@plan_onair');
$router->get('lackosong', 'ne_logController@lackosong');
$router->get('cikosong', 'ne_logController@cikosong');
$router->get('enodebid', 'ne_logController@enodebid');
$router->get('traffic_ci', 'ne_logController@traffic_ci');
$router->get('traffic_lac', 'ne_logController@traffic_lac');
$router->get('datasalah', 'ne_logController@datasalah');
$router->get('wrong_stylo', 'ne_logController@wrong_stylo');

//data btsonair for chart
$router->get('btsonair2g', 'ne_logController@btsonair2g');
$router->get('btsonair3g', 'ne_logController@btsonair3g');
$router->get('btsonair4g', 'ne_logController@btsonair4g');